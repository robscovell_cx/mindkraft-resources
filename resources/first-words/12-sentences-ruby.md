# 12 Golden Sentences
<p><ruby><rb>苹果是红色的<rt>Píngguǒ shì hóngsè de<rtc>The apple is red</ruby></p>
<p><ruby><rb>这是约翰的苹果<rt>zhè shì yuēhàn de píngguǒ<rtc>It is John&#39;s apple</ruby></p>
<p><ruby><rb>我给约翰苹果<rt>wǒ gěi yuēhàn píngguǒ<rtc>I give John the apple</ruby></p>
<p><ruby><rb>我们给他的苹果<rt>wǒmen gěi tā de píngguǒ<rtc>We give him the apple</ruby></p>
<p><ruby><rb>他把它交给约翰<rt>tā bǎ tā jiāo gěi yuēhàn<rtc>He gives it to John</ruby></p>
<p><ruby><rb>她把它交给他<rt>tā bǎ tā jiāo gěi tā<rtc>She gives it to him</ruby></p>
<p><ruby><rb>是苹果红？<rt>shì píngguǒ hóng?<rtc>Is the apple red?</ruby></p>
<p><ruby><rb>苹果是红色的<rt>Píngguǒ shì hóngsè de<rtc>The apples are red</ruby></p>
<p><ruby><rb>我必须把它给他<rt>wǒ bìxū bǎ tā gěi tā<rtc>I must give it to him</ruby></p>
<p><ruby><rb>我想给她<rt>wǒ xiǎng gěi tā<rtc>I want to give it to her</ruby></p>
<p><ruby><rb>我要知道明天<rt>wǒ yào zhīdào míngtiān<rtc>I&#39;m going to know tomorrow</ruby></p>
<p><ruby><rb>我不能吃苹果<rt>wǒ bùnéng chī píngguǒ<rtc>I can&#39;t eat the apple</ruby></p>
