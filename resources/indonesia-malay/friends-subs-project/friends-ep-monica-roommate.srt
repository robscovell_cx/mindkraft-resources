﻿1


00:00:05,755 --> 00:00:07,423
- Hai, teman2.
JOEY: Hey, Phoebe.

2
00:00:07,632 --> 00:00:08,924
Hey, oh, bagaimana hasilnya?

3
00:00:09,134 --> 00:00:10,175
Um, tak terlalu bagus.

4
00:00:10,385 --> 00:00:13,178
Dia mengantarku sampai ke subway dan bilang,
"Kita harus melakukan ini lagi."

5
00:00:13,388 --> 00:00:16,056
- Ouch.
- Apa? Dia bilang, "Kita harus melakukan ini lagi."

6
00:00:16,266 --> 00:00:17,307
Itu baik, kan?

7
00:00:17,517 --> 00:00:20,602
Uh, tidak. Arti dari
"Kita harus melakukan ini lagi," adalah: 

8
00:00:20,812 --> 00:00:22,855
"Kau tak akan pernah melihatku telanjang."

9
00:00:24,065 --> 00:00:25,274
Sejak kapan?

10
00:00:25,483 --> 00:00:28,819
Sejak dulu. Itu seperti, bahasa kencan.

11
00:00:29,029 --> 00:00:32,239
Kau tahu, seperti, "Bukan kau,"
berarti, "Memang kau."

12
00:00:33,616 --> 00:00:34,992
"Kau pria yg baik," berarti:

13
00:00:35,201 --> 00:00:37,161
"Aku akan mengencani alkoholik
berjaket kulit...

14
00:00:37,370 --> 00:00:39,788
...dan mengeluhkannya padamu."

15
00:00:40,623 --> 00:00:43,292
Atau, kau tahu, um, "Kurasa seharusnya
kita bertemu dengan orang lain," berarti:

16
00:00:43,501 --> 00:00:45,002
"Ha, ha, aku sudah melakukannya."

17
00:00:46,463 --> 00:00:47,880
Dan semua orang tahu ini?

18
00:00:48,089 --> 00:00:50,049
Oh, yeah.

19
00:00:50,258 --> 00:00:52,801
Seperti waktu kita kecil dan
orang tuamu harus "menidurkan" anjingmu...

20
00:00:53,011 --> 00:00:55,429
...dan mereka bilang dia dikirim
untuk tinggal di peternakan.

21
00:00:55,638 --> 00:00:58,682
Lucu.Tidak, karena,
uh, orang tua kami benar2...

22
00:00:58,892 --> 00:01:01,393
...uh, mengirim anjing kami ke peternakan.

23
00:01:01,603 --> 00:01:04,313
Uh, Ross?

24
00:01:05,148 --> 00:01:08,567
Ha, ha. Hello.
Peternakan Milner di Connecticut?

25
00:01:08,777 --> 00:01:11,195
The Milners, peternakan mereka
sangat luar biasa.

26
00:01:11,404 --> 00:01:13,947
Mereka punya kuda dan kelinci
yg bisa di kejar oleh anjingku.

27
00:01:14,157 --> 00:01:15,991
Dan itu...

28
00:01:18,578 --> 00:01:20,954
Oh, Tuhan. Chi-Chi.

29
00:02:15,218 --> 00:02:18,095
CHANDLER: "Jadi bagaimana rasanya
ketika mengetahui kau akan mati?"

30
00:02:18,304 --> 00:02:21,640
Penjaga, rasa sakitku akan
hilang dalam lima menit.

31
00:02:21,850 --> 00:02:26,728
Tapi kau hrs hidup dengan menyadari bahwa
kau telah menghukum mati org yg tak bersalah.

32
00:02:27,939 --> 00:02:29,648
- Hey, itu bagus sekali.
- Yeah?

33
00:02:29,858 --> 00:02:31,191
- Yeah.
- Makasih. Ayo kita lanjutkan.

34
00:02:31,401 --> 00:02:36,238
Okay. "Jadi, apa yg kau inginkan
dariku, Dimon? Huh?"

35
00:02:38,783 --> 00:02:42,661
Aku hanya ingin kembali ke selku,
karena disana aku bisa merokok.

36
00:02:42,871 --> 00:02:44,621
Pergilah.

37
00:02:52,505 --> 00:02:53,964
Heh.

38
00:02:55,633 --> 00:02:57,384
[BATUK]

39
00:02:57,886 --> 00:03:01,513
Kurasa itu sebabnya Dimon
merokok sendirian di selnya.

40
00:03:02,557 --> 00:03:04,391
- Apa?
- Lemaskan tanganmu.

41
00:03:04,601 --> 00:03:06,351
Lemaskan pergelangannya.

42
00:03:06,561 --> 00:03:07,603
- Jangan terlalu banyak.
- Whoa.

43
00:03:07,812 --> 00:03:09,062
Hey.

44
00:03:10,023 --> 00:03:12,107
Baiklah. Sekarang coba hisap.

45
00:03:12,567 --> 00:03:13,984
Benar.

46
00:03:15,862 --> 00:03:19,239
- Okay, tidak, berikan padaku.
- Tidak, tidak, tidak. Aku tak akan memberikanmu rokok ini.

47
00:03:19,449 --> 00:03:22,784
Tak apa2. Tak apa2. Dengar, kau mau
mendapatkan peran ini atau tidak? Kemarikan.

48
00:03:23,578 --> 00:03:26,622
Baiklah. Sekarang, jangan anggap
ini sebatang rokok.

49
00:03:26,831 --> 00:03:30,834
Bayangkan ini adalah sesuatu
yg telah hilang darimu.

50
00:03:31,669 --> 00:03:33,962
Ketika kau memegangnya,
kau merasa benar.

51
00:03:34,172 --> 00:03:36,506
- Kau merasa lengkap.
- Kau merindukannya?

52
00:03:36,716 --> 00:03:38,342
Tidak, tak terlalu.

53
00:03:40,094 --> 00:03:42,346
Baiklah. Sekarang kita merokok.

54
00:03:45,099 --> 00:03:47,601
Oh, Tuhan 

55
00:03:55,652 --> 00:03:56,693
Tidak, tidak, tidak.

56
00:03:56,903 --> 00:04:00,072
Mereka bilang jaraknya sama dengan
dari ujung ibu jari seseorang..

57
00:04:00,281 --> 00:04:01,907
...ke ujung telunjuk.

58
00:04:08,539 --> 00:04:10,290
Itu konyol.

59
00:04:11,709 --> 00:04:13,168
Bisa aku gunakan ibu jari yang lain?

60
00:04:15,213 --> 00:04:17,589
Baiklah. Jangan katakan padaku.

61
00:04:18,383 --> 00:04:21,260
Kapucino tanpa kafein utk Joey.

62
00:04:23,221 --> 00:04:24,972
Black Coffee.

63
00:04:26,099 --> 00:04:27,307
Latte.

64
00:04:29,435 --> 00:04:30,602
Dan segelas es teh.

65
00:04:31,229 --> 00:04:34,231
- Aku mulai ahli dalam hal ini.
- Bagus sekali. Itu bagus sekali.

66
00:04:34,440 --> 00:04:35,857
Baik untukku.

67
00:04:45,034 --> 00:04:46,451
[BERGUMAM]

68
00:04:49,622 --> 00:04:52,457
- Kau baik2 saja, Phoebe?
- Yeah, tidak, tidak sama sekali...

69
00:04:52,667 --> 00:04:54,001
Tentang bank-ku.

70
00:04:54,210 --> 00:04:55,460
Apa yg mereka lakukan paamu?

71
00:04:55,670 --> 00:04:57,504
Tak ada, semuanya... Okay.

72
00:04:57,714 --> 00:05:00,340
Aku menerima surat2ku,
lalu aku membaca catatan rekening dr mereka.

73
00:05:00,550 --> 00:05:02,009
Mudah.

74
00:05:04,137 --> 00:05:06,680
Dan ada kelebihan $500 dalam rekeningku.

75
00:05:06,889 --> 00:05:10,142
Oh, Iblis sedang bekerja lagi.

76
00:05:10,852 --> 00:05:13,312
Ya, karena sekarang aku harus
kesana dan membereskannya...

77
00:05:13,521 --> 00:05:16,732
- Kau bicara apa? Ambil saja uang itu.
- Itu bukan punyaku.

78
00:05:16,941 --> 00:05:19,568
Aku tak pantas mendapatkannya.
Jika kuambil sama saja dengan mencuri.

79
00:05:19,777 --> 00:05:23,363
Yeah, tapi jika kau habiskan,
itu namanya belanja.

80
00:05:24,574 --> 00:05:25,699
Okay. Okay.

81
00:05:25,908 --> 00:05:27,993
Katakanlah aku membeli 
sepasang sepatu mahal.

82
00:05:28,202 --> 00:05:30,495
Kau tahu apa yg akan kudengar
setiap kali aku melangkah?

83
00:05:30,705 --> 00:05:33,123
"Bukan punyaku. Bukan punyaku. Bukan punyaku."

84
00:05:33,333 --> 00:05:36,168
Dan walaupun jika aku sedang senang,
okay, dan meloncat2...

85
00:05:36,377 --> 00:05:39,546
...aku tetap mendengar, "Bukan punyaku. Bukan punyaku."

86
00:05:39,756 --> 00:05:41,381
Kami mendukungmu. Kami mengerti.

87
00:05:41,591 --> 00:05:46,636
Okay, aku tak akan menikmatinya.
Itu seperti berhutang karma yg bear.

88
00:05:46,846 --> 00:05:49,222
Chandler, apa yg kau lakukan? Hey.

89
00:05:49,432 --> 00:05:51,266
Apa yg sedang kau lakukan?

90
00:05:54,312 --> 00:05:56,355
Oh, menjijikan.

91
00:05:57,065 --> 00:05:59,399
- Apa ini?
- Aku merokok. Aku merokok. Aku merokok.

92
00:05:59,609 --> 00:06:02,194
Aku tak percaya.
Kau sudah 3 tahun berhenti.

93
00:06:02,403 --> 00:06:04,988
Dan inilah hadiahku.

94
00:06:06,282 --> 00:06:09,659
Sebentar. Pikirkan apa yg harus
kau alami ketika terakhir kau berhenti.

95
00:06:09,869 --> 00:06:11,745
Okay, kalau begitu kali ini
aku tidak akan berhenti.

96
00:06:11,954 --> 00:06:14,456
- Buang.
- Baiklah. Aku buang.

97
00:06:15,750 --> 00:06:17,376
Oh, tidak.

98
00:06:17,585 --> 00:06:19,544
Aku tak dapat minum ini sekarang.

99
00:06:23,591 --> 00:06:25,675
Baiklah, aku harus ganti baju.
Aku ada janji kencan.

100
00:06:25,885 --> 00:06:28,345
Dengan Alan lagi? Bagaimana hasilnya?

101
00:06:28,554 --> 00:06:32,682
Berjalan lancar, kau tahu?
Baik dan kami menikmatinya.

102
00:06:32,892 --> 00:06:34,851
- Jadi kapan kami bisa bertemu lelaki ini?
ROSS: Yeah.

103
00:06:35,061 --> 00:06:36,812
Coba lihat, hari ini Senin...

104
00:06:37,021 --> 00:06:38,897
- Tak akan pernah.
- Ayolah.

105
00:06:39,107 --> 00:06:42,109
- Tidak, tidak setelah apa yg terjadi pada Steve.
- Bicara apa kau?

106
00:06:42,318 --> 00:06:45,779
Kami suka Sssteve. Sssteve sssexy.

107
00:06:48,699 --> 00:06:49,741
[DENGAN SUARA NORMAL]
Sorry.

108
00:06:49,951 --> 00:06:53,078
Dengar, aku belum tahu bagaimana perasaanku padanya.
Beri aku kesempatan untuk memutuskannya.

109
00:06:53,287 --> 00:06:54,913
Well, lalu kami bisa bertemu dengannya?

110
00:06:55,415 --> 00:06:56,456
Tidak.

111
00:06:56,666 --> 00:06:58,250
Sssorry.

112
00:07:01,712 --> 00:07:03,171
Mengapa aku harus mempertemukan
mereka dengan pria ini?

113
00:07:03,381 --> 00:07:06,299
Aku mengajak seorang pria ke rumah dan
dalam 5 menit mereka semua menggodanya.

114
00:07:06,509 --> 00:07:10,345
Maksudku, mereka seperti coyote yg
sedang memilih anggota kawanan yg terlemah.

115
00:07:11,055 --> 00:07:15,392
Dengar, sebagai seseorang yg
lebih terlihat seperti daging sapi busuk...

116
00:07:15,601 --> 00:07:19,396
...kuberitahu kau,
ini bukan hal yg buruk.

117
00:07:19,605 --> 00:07:22,107
Ayolah, mereka temanmu.
Mereka menjagamu.

118
00:07:22,316 --> 00:07:25,652
Aku tahu, aku tahu. Aku hanya berharap mereka
akhirnya menyukai seseorang yg kubawa pulang.

119
00:07:25,862 --> 00:07:29,573
Well, kau sadar bahwa
peluang itu sangat kecil...

120
00:07:29,782 --> 00:07:31,616
...jika mereka tak pernah
bertemu dengan pria ini.

121
00:07:33,578 --> 00:07:35,120
Relakan, Ross.

122
00:07:35,329 --> 00:07:37,080
Yeah? Well, kau tak kenal Chi-Chi.

123
00:07:41,252 --> 00:07:42,419
MONICA:
Kalian semua janji?

124
00:07:42,628 --> 00:07:45,338
- Yeah, kami janji. Kami akan menjaga sikap.
JOEY: Kami janji.

125
00:07:45,548 --> 00:07:48,467
Chandler, kau janji akan menjaga sikapmu?

126
00:07:51,095 --> 00:07:52,637
[SUARA GUNTUR]

127
00:07:56,058 --> 00:08:00,270
Kau boleh masuk tapi kau
harus meninggalkan kawanmu di luar.

128
00:08:06,694 --> 00:08:08,195
ROSS:
Hey, Pheebs.

129
00:08:10,072 --> 00:08:13,241
"Dear Ms. Buffay: Terima kasih sudah
menginformasikan kesalahan kami.

130
00:08:13,451 --> 00:08:17,579
Kami telah mengkredit (menambahkan) $500 di rekening anda.
Maaf akan ketidaknyamanan ini...

131
00:08:17,788 --> 00:08:20,665
...dan kami berharap anda bersedia
menerima telepon berbentuk football ini...

132
00:08:23,920 --> 00:08:25,921
...sebagai hadiah dr kami."
Kalian percaya ini?

133
00:08:26,130 --> 00:08:29,382
Sekarang aku punya $1000
dan sebuah telepon football.

134
00:08:31,052 --> 00:08:33,345
Bank apa ini?

135
00:08:33,554 --> 00:08:34,638
[BEL PINTU]

136
00:08:34,847 --> 00:08:36,389
Okay, dia datang.

137
00:08:38,059 --> 00:08:39,100
- Siapa?
ALAN: Alan.

138
00:08:39,310 --> 00:08:41,102
Chandler. Dia datang.

139
00:08:44,232 --> 00:08:46,608
Okay, tolong baik2 lah. Kumohon?

140
00:08:46,817 --> 00:08:48,527
Maksudku, ingat bagaimana
kalian menyukaiku.

141
00:08:48,736 --> 00:08:50,237
[KETUKAN DI PINTU]

142
00:08:50,947 --> 00:08:54,491
Hai. Alan, perkenalkan seua.
Semuanya, ini Alan.

143
00:08:56,118 --> 00:08:58,370
- Hai.
GROUP: Hey.

144
00:08:59,539 --> 00:09:02,457
Aku sssering mendengar tentang
kalian sssemua.

145
00:09:08,881 --> 00:09:11,174
MONICA:
Makasih, aku akan meneleponmu besok.

146
00:09:14,262 --> 00:09:15,679
Okay.

147
00:09:15,888 --> 00:09:19,307
Okay, sekarang waktunya utk
mempermalukan-Alan dimulai.

148
00:09:19,517 --> 00:09:21,351
Siapa yg pertama?

149
00:09:21,561 --> 00:09:22,727
Hmm?

150
00:09:23,145 --> 00:09:24,521
Ayolah.

151
00:09:25,439 --> 00:09:26,898
Aku mulai.

152
00:09:29,569 --> 00:09:31,987
Kita mulai dengan cara dia
memilih...

153
00:09:32,196 --> 00:09:33,655
Kau tahu, aku minta maaf,
aku tak dapat melakukan ini.

154
00:09:33,864 --> 00:09:35,323
Tak bisa. Kami menyukainya.

155
00:09:35,533 --> 00:09:38,118
GROUP: Kami menyukainya.
- Sebentar.

156
00:09:38,327 --> 00:09:40,412
Kita sedang membicarakan
seseorang yg sedang kukencani?

157
00:09:40,621 --> 00:09:42,414
GROUP: Ya.
- Dan apa kau menyadari?

158
00:09:42,623 --> 00:09:43,957
GROUP:
Yeah.

159
00:09:48,504 --> 00:09:51,464
Tahu apa yg hebat?
Cara dia senyum yg agak bengkok?

160
00:09:51,674 --> 00:09:53,675
Ya, ya. Seperti pria dalam sepatu.

161
00:09:54,552 --> 00:09:55,677
Sepatu apa?

162
00:09:57,096 --> 00:09:58,430
Dari puisi nursery.

163
00:09:58,639 --> 00:10:00,890
"There was a crooked man
who had a crooked smile...

164
00:10:01,100 --> 00:10:03,727
...who lived in a shoe for a while..."

165
00:10:06,814 --> 00:10:09,024
Jadi kurasa Alan...

166
00:10:09,233 --> 00:10:10,442
...akan menjadi tolok ukur...

167
00:10:10,651 --> 00:10:13,403
...untuk menilai pacar2mu
yg berikutnya.

168
00:10:13,613 --> 00:10:14,779
Pacarku yg berikutnya apa?

169
00:10:14,989 --> 00:10:17,532
Tidak, tidak. Kurasa yg ini mungkin
menjadi, kau tahu, "sesuatu."

170
00:10:17,742 --> 00:10:18,825
- Sungguh?
- Oh, yeah.

171
00:10:19,035 --> 00:10:22,495
Aku bisa menikahinya hanya karena
caranya menirukan David Hasselhoff.

172
00:10:22,705 --> 00:10:25,332
Kau tahu aku berniat melakukan
itu di pesta kan?

173
00:10:29,128 --> 00:10:31,630
- Kau tahu apa yg paling kusuka darinya?
- Apa?

174
00:10:32,465 --> 00:10:35,675
Caranya dia membuatku merasa percaya diri.

175
00:10:38,512 --> 00:10:40,055
ALL:
Yeah.

176
00:10:53,277 --> 00:10:54,861
Hai.

177
00:10:57,198 --> 00:10:58,365
Bagaimana pertandingannya?

178
00:10:59,742 --> 00:11:01,117
Well...

179
00:11:01,327 --> 00:11:03,870
- Kami menang!
- Kita menang! Terima kasih! Yeah.

180
00:11:04,705 --> 00:11:07,624
Fantastis. Satu pertanyaan:
Bagaimana mungkin?

181
00:11:08,709 --> 00:11:11,002
- Alan.
- Dia tak bisa dipercaya.

182
00:11:11,212 --> 00:11:14,756
Dia seperti tokoh kartun Bugs Bunny
dimana si Bugs bermain di semua posisi.

183
00:11:14,965 --> 00:11:18,426
Tapi dr pd Bugs, itu lebih ke:
base satu, Alan. Base dua, Alan...

184
00:11:22,056 --> 00:11:25,475
Maksudku, seperti dia
menyatukan tim.

185
00:11:25,685 --> 00:11:27,686
Yep. Kami jelas seperti mengajarkan
tim Hasidic jewelers...

186
00:11:27,895 --> 00:11:30,522
...satu atau dua hal tentang softball.
- Mantap.

187
00:11:31,857 --> 00:11:33,441
Bisa aku bertanya pada kalian?

188
00:11:33,651 --> 00:11:36,111
Kalian pernah merasa kadang2
Alan agak sedikit...

189
00:11:36,320 --> 00:11:40,281
- Apa?
- Aku tak tahu. Sedikit terlalu Alan?

190
00:11:40,491 --> 00:11:43,910
Oh, tidak. Itu tak mungkin.
Kau tak akan pernah terlalu Alan.

191
00:11:44,120 --> 00:11:48,832
Yeah, justru ke-Alan-nan nya
yg kami suka.

192
00:11:49,166 --> 00:11:51,376
- Aku pribadi, bisa menghabiskan segalon Alan.
- Oh!

193
00:11:57,383 --> 00:12:00,218
- Hey, Lizzy.
- Hey, gadis aneh.

194
00:12:00,428 --> 00:12:03,138
- Kubawakan kau sup alfabet.
- Kau pilih yg vokal?

195
00:12:03,347 --> 00:12:06,349
Ya, tapi aku tak mengambil yg 'Y'.
Karena, kau tahu, "Kadang2 Y."

196
00:12:07,143 --> 00:12:08,768
Um, Aku juga punya sesuatu utkmu.

197
00:12:08,978 --> 00:12:10,061
Garam?

198
00:12:10,271 --> 00:12:12,647
Tidak, tapi apakah  kau mau $1000
dan sebuah telepon football?

199
00:12:13,232 --> 00:12:14,607
Apa?

200
00:12:15,735 --> 00:12:17,318
Oh, Tuhan 

201
00:12:17,570 --> 00:12:20,196
- Oh, Tuhan ini uang sungguhan
- Aku tahu.

202
00:12:20,406 --> 00:12:22,115
Gadis aneh, apa yg kau lakukan?

203
00:12:22,324 --> 00:12:24,242
Oh, Aku ingin kau menerimanya.
Aku tak menginginkannya.

204
00:12:24,952 --> 00:12:27,412
Tidak, tidak. Aku harus memberikan sesuatu padamu.

205
00:12:27,621 --> 00:12:29,247
Tidak, tak apa. Kau tak perlu...

206
00:12:29,457 --> 00:12:31,374
Kau mau topi tinfoil ku?

207
00:12:31,584 --> 00:12:34,461
Tidak, kau memerlukannya.
Tidak, tak apa2. Makasih.

208
00:12:34,670 --> 00:12:36,796
Kumohon. Biar aku melakukan sesuatu utkmu.

209
00:12:37,423 --> 00:12:38,840
Okay, Baiklah. Kuberitahu kau.

210
00:12:39,049 --> 00:12:41,551
Traktir aku soda lalu kita impas. Okay?

211
00:12:42,511 --> 00:12:43,803
- Okay.
PHOEBE: Okay.

212
00:13:45,825 --> 00:13:47,283
Ambil kembaliannya.

213
00:13:48,035 --> 00:13:49,077
Makasih, Lizzy.

214
00:13:49,286 --> 00:13:50,453
Yakin kau tak mau pretzel?

215
00:13:50,663 --> 00:13:53,665
- Tidak, terima kasih.
- Sampai jumpa lagi.

216
00:13:59,338 --> 00:14:01,214
Huh.

217
00:14:01,590 --> 00:14:02,924
Sebuah ibu jari?

218
00:14:03,133 --> 00:14:04,676
GROUP:
Ew.

219
00:14:05,052 --> 00:14:07,220
Aku tahu, aku tahu.
Aku membukanya dan itu...

220
00:14:07,429 --> 00:14:10,849
...mengambang di dlm situ,
seperti penumpang kecil.

221
00:14:12,768 --> 00:14:15,395
Mungkin ini semacam sayembara, kau tahu?
Seperti "Kumpulkan kelima-limanya."

222
00:14:17,940 --> 00:14:19,148
Ada, um, yang mau lihat?

223
00:14:19,358 --> 00:14:20,775
Tidak, Makasih.

224
00:14:21,777 --> 00:14:24,237
- Oh, hey, jangan lakukan itu. Ayolah.
- Hentikan.

225
00:14:24,446 --> 00:14:25,947
Ini lebih buruk dari ibu jari.

226
00:14:27,741 --> 00:14:29,534
Hey, ini tdk adil.

227
00:14:29,743 --> 00:14:34,038
- Mengapa tak adil?
- Jadi aku punya kekurangan. Tak masalah.

228
00:14:34,248 --> 00:14:36,958
Seperti Joey yg selalu membunyikan
buku jarinya, tidakkah itu mengganggu.?

229
00:14:37,167 --> 00:14:39,335
Dan Ross, dengan penyebutan setiap kata
yg berlebihan.

230
00:14:39,545 --> 00:14:41,671
Dan Monica, yg mendengus ketika dia tertawa.

231
00:14:41,881 --> 00:14:44,215
Maksudku, apa itu semua?

232
00:14:46,176 --> 00:14:49,470
Aku bisa menerima semua kekurangan itu.
Kenapa kalian tak bisa menerima kekuranganku?

233
00:14:56,645 --> 00:14:59,898
Apakah membunyikan buku jari
mengganggu bagi yg lain, atau hanya dia?

234
00:15:01,984 --> 00:15:03,359
Well...

235
00:15:03,819 --> 00:15:05,528
...aku bisa hidup dengannya.

236
00:15:06,572 --> 00:15:07,739
Huh.

237
00:15:08,449 --> 00:15:13,119
Well, apakah itu, sedikit mengganggu? atau
seperti waktu Phoebe mengunyah rambutnya?

238
00:15:15,956 --> 00:15:19,083
Jangan dengarkan dia, Pheebs, Oke?
Kurasa itu menawan.

239
00:15:19,293 --> 00:15:21,544
Oh, benarkah?

240
00:15:22,880 --> 00:15:23,922
[TERTAWA]

241
00:15:24,131 --> 00:15:25,381
[MENDENGUS]

242
00:15:27,593 --> 00:15:30,845
Kau tahu, tak ada salahnya 
bicara dengan benar.

243
00:15:31,055 --> 00:15:33,848
Tepat sekali.

244
00:15:36,018 --> 00:15:37,769
Aku harus kembali bekerja.

245
00:15:37,978 --> 00:15:41,064
Yeah, kalau tidak seseorang mungkin
akan mendapat pesanan mereka dengan benar.

246
00:15:42,358 --> 00:15:46,694
Oh. Rambutnya keluar dan
sarung tangannya terlepas.

247
00:15:47,279 --> 00:15:48,863
[SEMUA MENGOMEL]

248
00:16:02,252 --> 00:16:05,838
Benarkah kau berkencan dengan 
pria yg teman-temanmu suka?

249
00:16:06,215 --> 00:16:07,465
Tidak.

250
00:16:08,926 --> 00:16:12,887
Okay. Well, aku berkencan dengan 
pria yg teman-temanku suka.

251
00:16:14,348 --> 00:16:16,724
Kita sedang membicarakan para coyote itu?

252
00:16:16,934 --> 00:16:20,395
Heh. Baiklah. Seekor sapi lewat.

253
00:16:21,480 --> 00:16:22,772
Kau percaya?

254
00:16:22,982 --> 00:16:24,565
Ini cuma, apa ya?

255
00:16:24,775 --> 00:16:27,735
Aku tidak merasa ada sesuatu.

256
00:16:27,945 --> 00:16:30,530
Maksudku, teman2ku merasakan sesuatu.
Aku tidak.

257
00:16:30,781 --> 00:16:33,366
Honey, kau selalu...

258
00:16:33,575 --> 00:16:35,535
...merasakan sesuatu.

259
00:16:38,122 --> 00:16:40,915
Dengar, jika itu yg kau rasakan
pada pria itu, Monica, putuskan dia.

260
00:16:41,125 --> 00:16:43,042
Aku tahu, cuma akan sulit.

261
00:16:43,252 --> 00:16:45,128
Yeah, dia sudah dewasa.
Dia akan mampu menghadapinya.

262
00:16:45,337 --> 00:16:50,008
Tidak, dia pasti akan bak2 saja.
Yang lima yg lain yg kukhawatirkan.

263
00:16:50,801 --> 00:16:52,343
Apakah kau menghargai tubuhmu?

264
00:16:52,553 --> 00:16:54,637
Tidak sadarkah kau dengan
yg kau lakukan pada tubuhmu?

265
00:16:54,847 --> 00:16:57,098
Hey, kau tahu, cukup denganmu
dan kankermu...

266
00:16:57,307 --> 00:16:59,100
...dan radang paru2mu
dan sakit jantungmu.

267
00:16:59,309 --> 00:17:02,937
Yang jelas, merokok itu keren,
dan kau tahu itu.

268
00:17:05,441 --> 00:17:06,816
Ahem. Chandler?

269
00:17:07,026 --> 00:17:08,776
Ini Alan. Dia mau bicara denganmu.

270
00:17:08,986 --> 00:17:10,820
Sungguh?

271
00:17:12,239 --> 00:17:14,407
Hey, sobat. Ada apa?

272
00:17:16,035 --> 00:17:18,369
Oh, dia bilang itu padamu, huh?

273
00:17:19,163 --> 00:17:23,166
Well, yeah. Aku punya 1.
Well, yeah, sekarang.

274
00:17:24,084 --> 00:17:25,877
Well, tak terlalu buruk...

275
00:17:28,130 --> 00:17:29,672
Well, itu betul.

276
00:17:32,384 --> 00:17:36,179
Gee, kau tahu, tak seorangpun
yg pernah mengatakannya seperti itu.

277
00:17:36,597 --> 00:17:38,723
Well, okay. Makasih.

278
00:17:49,109 --> 00:17:50,151
Tuhan, dia hebat.

279
00:17:51,987 --> 00:17:53,404
Jika saja dia itu wanita.

280
00:17:53,614 --> 00:17:55,406
Yeah.

281
00:18:00,496 --> 00:18:02,121
CHANDLER:
Oh, Lamb Chop.

282
00:18:02,331 --> 00:18:04,957
Sudah berapa lama kaus kaki itu?

283
00:18:06,001 --> 00:18:09,921
Jika aku punya kaus kaki
selama 30 tahun, ia pasti bicara juga.

284
00:18:11,465 --> 00:18:16,052
Okay, kurasa ini waktunya bagi
seseorang utk merubah nicotine patch nya.

285
00:18:17,429 --> 00:18:18,846
Hey.

286
00:18:19,765 --> 00:18:20,807
Dimana Joey?

287
00:18:21,016 --> 00:18:24,143
Joey memakan permen karetku yg terakhir
jadi kubunuh dia.

288
00:18:25,771 --> 00:18:27,688
Ku pikir itu salah?

289
00:18:29,441 --> 00:18:32,151
- Kurasa dia diseberang koridor.
- Makasih.

290
00:18:32,694 --> 00:18:34,195
Ini dia.

291
00:18:34,988 --> 00:18:37,240
Ooh, aku hidup dengan kenikmatan sekarang.

292
00:18:38,325 --> 00:18:41,077
Hey, Pheebs, kau akan makan
sisa Pop Tart itu?

293
00:18:43,122 --> 00:18:45,915
Ada yg mau Pop Tart ini?

294
00:18:47,501 --> 00:18:48,918
Hey, aku mungkin mau.

295
00:18:50,003 --> 00:18:51,337
Aku minta maaf.

296
00:18:51,922 --> 00:18:55,383
Kau tahu, penjual soda bodoh itu
memberiku $7000 untuk sebuah ibu jari.

297
00:18:55,592 --> 00:18:57,635
- Oh, Tuhan 
- Tujuh ribu dollar?

298
00:18:57,845 --> 00:19:00,221
Dan ketika aku sedang kesini,
aku menginjak permenkaret.

299
00:19:01,640 --> 00:19:03,850
Ada apa dengan dunia ini?

300
00:19:04,059 --> 00:19:05,143
Apa yg terjadi?

301
00:19:05,352 --> 00:19:08,187
Tak ada. Aku hanya berpikir sungguh
menyenangkan bila sedang berkumpul begini.

302
00:19:08,397 --> 00:19:11,524
Lebih menyenangkan lagi jika seseorang 
memakai pakaian dalamnya.

303
00:19:14,444 --> 00:19:17,155
- Uh, Joey...
- Oh.

304
00:19:19,741 --> 00:19:21,951
MONICA: Okay.
- Oh, ayolah.

305
00:19:22,161 --> 00:19:24,120
Kumohon, kawan2, kita harus bicara.

306
00:19:24,329 --> 00:19:27,123
Tunggu, tunggu. Aku mengalami déjà vu.

307
00:19:27,332 --> 00:19:28,624
Aku tidak.

308
00:19:31,503 --> 00:19:33,629
- Baiklah, kita harus bicara.
- Nah yg ini baru.

309
00:19:37,050 --> 00:19:38,885
Okay, ini tentang Alan.

310
00:19:39,553 --> 00:19:41,846
Ada sesuatu yg harus kalian ketahui.

311
00:19:43,348 --> 00:19:47,393
Oh, man, sungguh tak mudah untuk
mengatakan hal ini. Uh...

312
00:19:48,395 --> 00:19:50,813
Aku memutuskan untuk putus dengan Alan.

313
00:19:55,360 --> 00:19:57,361
Apakah ada orang ketiga?

314
00:19:59,781 --> 00:20:01,741
Tidak, tidak, tidak. Hanya saja...

315
00:20:01,950 --> 00:20:04,952
...kau tahu, ada yg berubah.
Manusia berubah.

316
00:20:05,329 --> 00:20:07,205
Kami tidak berubah.

317
00:20:09,791 --> 00:20:11,709
Jadi begitu saja? sudah selesai?

318
00:20:13,337 --> 00:20:14,712
Begitu saja?

319
00:20:15,797 --> 00:20:17,548
Kau tahu, kau lengah.

320
00:20:17,758 --> 00:20:20,843
Kau tahu, kau mulai perhatian
dengan seseorang, dan aku...

321
00:20:24,348 --> 00:20:26,933
- Dengar, aku isa pura2...
- Okay.

322
00:20:27,142 --> 00:20:29,393
Tidak. tidak, itu tak adil buatku...

323
00:20:29,603 --> 00:20:31,395
...itu tak adil buat Alan,
atau untukmu.

324
00:20:31,688 --> 00:20:33,689
Yeah, well, siapa yg mau adil?

325
00:20:33,899 --> 00:20:37,777
Maksudku, aku hanya ingin semuanya kembali,
kau tahu, seperti dulu.

326
00:20:39,905 --> 00:20:43,908
- Aku minta maaf.
- Oh, dia minta maaf. aku merasa lebih baik.

327
00:20:45,661 --> 00:20:47,078
Aku tak percaya ini.

328
00:20:47,287 --> 00:20:51,958
Maksudku, dengan liburan yg segera datang.
Aku ingin mempertemukannya dengan keluargaku.

329
00:20:53,126 --> 00:20:56,170
Aku akan menemukan orang lain.
Akan ada Alan-Alan yg lain.

330
00:20:56,380 --> 00:20:57,546
Yeah, benar.

331
00:21:02,427 --> 00:21:05,972
- Kalian akan baik2 saja?
- Hey, hey. kami akan baik2 saja.

332
00:21:06,181 --> 00:21:08,808
Kami hanya perlu waktu sedikit.

333
00:21:09,726 --> 00:21:11,310
Aku ngerti.

334
00:21:16,441 --> 00:21:17,858
Wow.

335
00:21:18,068 --> 00:21:21,988
- aku benar2 minta maaf.
- Yeah. Maksudku, aku juga minta maaf.

336
00:21:22,614 --> 00:21:24,782
Tapi harus kuakui, aku agak sedikit lega.

337
00:21:24,992 --> 00:21:26,867
- Lega?
- Yeah, well...

338
00:21:27,077 --> 00:21:29,412
...maksudku, aku mendapatkan waktu yg
menyenangkan denganmu.

339
00:21:30,080 --> 00:21:31,831
Aku hanya tak tahan dengan teman2mu.

340
00:21:39,172 --> 00:21:42,383
Ingat ketika kita pergi ke
Central Park dan menyewa perahu?

341
00:21:44,428 --> 00:21:46,178
Itu menyenangkan.

342
00:21:46,722 --> 00:21:50,266
Yeah. Dia mendayung seperti orang viking.

343
00:21:55,105 --> 00:21:57,106
- Hai.
GROUP: Hai.

344
00:21:59,067 --> 00:22:00,943
Jadi gimana hasilnya?

345
00:22:02,029 --> 00:22:04,947
- Kau tahu.
- Apakah dia menyebut2 tentang kami?

346
00:22:10,370 --> 00:22:12,955
Dia bilang dia akan merindukan kalian.

347
00:22:19,629 --> 00:22:20,838
ROSS:
Harimu buruk, ya?

348
00:22:21,048 --> 00:22:23,841
- Oh, kau tak tahu saja.
ROSS: Kemarilah..

349
00:22:27,471 --> 00:22:29,764
- Cukup. Aku akan merokok.
GROUP: Tidak, tidak, tidak.

350
00:22:29,973 --> 00:22:31,223
Aku tak perduli. Aku tak perduli.

351
00:22:31,433 --> 00:22:34,560
Game over. Aku lemah. Aku harus merokok.
Aku harus merokok.

352
00:22:34,770 --> 00:22:37,355
Jika kau tak pernah merokok lagi,
aku akan memberimu $7000.

353
00:22:38,607 --> 00:22:40,024
Yeah, Baiklah.

