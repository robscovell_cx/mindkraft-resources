"Kita Lihat Apakah Amerika akan Menyelamatkan Kalian Sekarang!"

Sabtu, 29 Mei 2021 07:35 Reporter : Pandasurya Wijaya
"Kita Lihat Apakah Amerika akan Menyelamatkan Kalian Sekarang!" Seorang
perempuan Tigray, Ethiopia, menggendong anaknya. ©Mohamed Nureldin Abdallah/
Reuters

Merdeka.com - Beberapa hari setelah Amerika Serikat mengumumkan sanksi
finansial dan larangan visa bagi para pejabat Ethiopia dan Eritrea, saksi mata
mengatakan kepada CNN, ratusan pemuda dikumpulkan dari sejumlah kamp pengungsi
di Kota Shire, Tigray pada Senin malam.

Saksi yang enggan diketahui identitasnya mengatakan kepada CNN tentang
bagaimana tentara Ethiopia dan Eritrea menyerbu sedikitnya dua pusat
pengungsian kemudian memukuli dan menyiksa warga Tigray. Konflik di Ethiopia
ini diyakini sudah membunuh ribuan warga sipil sejak November tahun lalu. Saksi
mengungkapkan tentara membawa pergi ratusan orang dari pengungsian.

Menurut penuturan saksi, empat kendaraan militer mengepung kamp pengungsian Adi
Wenfito dan Tsehay kemudian mereka mengumpulkan para pemuda, memaksa mereka
naik ke dalam bus dan membawa mereka ke sebuah lokasi yang diyakini berada di
pinggiran Shire. Ketika tentara menyerbu sekolah yang dijadikan pengungsian,
saksi mendengar mereka berteriak, "Kita lihat apakah Amerika akan menyelamatkan
kalian sekarang!"

"Mereka menjebol pintu, orang-orang itu bahkan belum sempat memakai sepatu.
Tentara sudah menodongkan senapan ke arah mereka," kata seorang saksi, seperti
dilansir CNN, Kamis (27/5).

Seorang perempuan mengatakan dua putranya--berusia 19 dan 24 tahun--dibawa
pergi dari rumah mereka sekitar pukul 21.30 malam itu. "Mereka tidak bilang
mengapa anak saya dibawa. Mereka mengikat anak saya, memukuli dan membawa
pergi," kata dia kepada CNN.

Sejumlah pemuda yang sempat dikumpulkan kemudian dibebaskan pada sore hari
Selasa lalu setelah mereka mengaku sebagai pekerja kemanusiaan. Mereka
mengatakan ratusan anak muda terus ditangkapi di Guna, pusat distribusi bantuan
kemanusiaan yang kini sudah menjadi sebuah kamp militer.

Seorang pemuda menggambarkan bagimana mereka dipukuli berjam-jam oleh tentara
Eritrea dan Ethiopia.

"Kebanyakan kami masih muda tapi ada orang yang lebih tua yang tidak kuat
dipukuli lama-lama," kata dia.

Menteri Penerangan Eritrea Yemene Ghebremeskel menyangkal laporan semacam itu
dan membantah laporan CNN dengan mengatakan, "Sampai kapan Anda akan terus
percaya omongan saksi. Kami sering mendengar cerita bohong dari mereka."

Presiden Amerika Serikat Joe Biden dalam pernyataannya Rabu lalu mengatakan dia
"sangat prihatin dengan kekerasan yang terus meningkat" di Ethiopia dan
mengecam "pelanggaran HAM dalam skala besar yang terjadi di Tigray."

Konflik di Tigray kini sudah berlangsung selama 200 hari. Barisan Pembebasan
Rakyat Tigray (TPLF) melawan Pasukan Pertahanan Nasional Ethiopia, tentara
Eritrea dan milisi dari etnis Amhara. Sejak dimulainya konflik, warga sipil
menjadi target dari pasukan pemerintah dan tentara Eritrea serta milisi Amhara.

Lembaga kemanusiaan memperkirakan Kota Shire kini semakin padat karena
menampung sekitar 800.000 warga Tigray yang mengungsi karena pertempuran di
wilayah Barat. Menteri Luar Negeri AS Antony Blinken mengatakan kekerasan yang
dilakukan pasukan pemerintah Ethiopia beserta sekutunya tentara Eritrea dan
milis Amhara adalah "pembersihan etnis".

CNN sudah menghubungi kantor Perdana Menteri Ethiopia dan Menteri Penerangan
Eritrea namun belum ada balasan.

Dalam sebuah video yang diterima CNN pada Selasa pagi, gambar yang direkam
diam-diam itu memperlihatkan sejumlah orangtua yang berkumpul di kantor PBB
Urusan pengungsi, UNHCR. Dalam satu video tentara Ethiopia terlihat sedang
berbicara di depan para orangtua.

Suara di video itu kurang jelas namun sejumlah saksi mengatakan para orangtua
itu diberitahu oleh tentara: "Kami bisa membunuh kalian di sini sekarang dan
PBB tidak bisa berbuat apa-apa selain mengambil foto kalian." [pan]

“Rahim Orang Tigray Seharusnya Tidak Pernah Melahirkan"Pengakuan Mengerikan Tim
Medis yang Tangani Perempuan Korban Pemerkosaan di TigrayPBB: Kekerasan Seksual
Jadi Senjata Perang di TigrayPasukan Eritrea Mulai Ditarik dari Wilayah Tigray
Ethiopia"Dua Peluru Cukup", Foto Satelit Buktikan Tentara Ethiopia Bantai Warga
di Tigray
